(function($, window, document, Drupal) {

	function Banner(banner) {
		this.slides = banner.find('.banner__background-item');
		this.slide;
		this.sliderTimer;
		this.index = 0;
		this.init();
	}

	Banner.prototype.init = function() {
		this.loadSlide();
	}
	Banner.prototype.isUnloaded = function(slide) {
		return slide.hasClass('banner__background-item--loading');
	}
	Banner.prototype.changeSlide = function() {
		var banner = this;

		// Change classes so the css transitions the images
		banner.slides.removeClass('banner__background-item--active');
		banner.slide.removeClass('banner__background-item--loading').addClass('banner__background-item--active');

		// Increment index and restart timer when there is more than 1 slide
		if(banner.slides.length > 1) {
			banner.index++;
			banner.sliderTimer = setTimeout(function() { banner.loadSlide() }, 4000);
		}
	}
	Banner.prototype.loadSlide = function() {
		var banner = this;
		
		// Reset index when we've reached the end
		if(banner.index >= banner.slides.length) {
			banner.index = 0;
		}

		// Select the slide we want to show
		banner.slide = banner.slides.eq(banner.index)

		// Lazy load images before changing the slide
		if(banner.isUnloaded(banner.slide)) {
			img = banner.slide.find('img');
			img.on('load', function() {
				banner.changeSlide();
			});
			if($(window).width() < 480) {
				if('objectFit' in document.documentElement.style === false) {
					banner.slide.css('background-image', 'url(' + banner.slide.data('bg-mobile') + ')');
					img.remove();
					banner.changeSlide();
				} else {
					img.attr('src', banner.slide.data('bg-mobile'));
				}
			} else if($(window).width() < 960) {
				if('objectFit' in document.documentElement.style === false) {
					banner.slide.css('background-image', 'url(' + banner.slide.data('bg-tablet') + ')');
					img.remove();
					banner.changeSlide();
				} else {
					img.attr('src', banner.slide.data('bg-tablet'));
				}
			} else {
				if('objectFit' in document.documentElement.style === false) {
					banner.slide.css('background-image', 'url(' + banner.slide.data('bg') + ')');
					img.remove();
					banner.changeSlide();
				} else {
					img.attr('src', banner.slide.data('bg'));
				}
			}

		// Image is already loaded, change the slide instantly
		} else {
			banner.changeSlide();
		}
	}

	$(function() {
		$('.banner').each(function() {
			var banner = new Banner($(this));
		});
	});

})(jQuery, window, document, Drupal);